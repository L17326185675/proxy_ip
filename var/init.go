package _var

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

var MySqlDB *gorm.DB
var CONFIG = new(ServerCfg)

type ServerCfg struct {
	ConfigFile      string `mapstructure:"config_file" json:"config_file" yaml:"config_file"`
	MysqlLink       string `mapstructure:"mysql_link" json:"mysql_link" yaml:"mysql_link"`
	PingIp          string `mapstructure:"ping_ip" json:"ping_ip" yaml:"ping_ip"`
	Ipv4Header      int    `mapstructure:"ipv4_start" json:"ipv4_start" yaml:"ipv4_start"`
	ChanelCapacity int `mapstructure:"chanel_cap" json:"chanel_cap" yaml:"chanel_cap"`
	GoroutineNumber int    `mapstructure:"goroutine_number" json:"goroutine_number" yaml:"goroutine_number"`
}

func initConfigFromYaml() {
	file, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		//fmt.Println(err)
		panic(err)
	}
	err = yaml.Unmarshal(file, CONFIG)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(CONFIG)
}

func init() {
	initConfigFromYaml()
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN: CONFIG.MysqlLink, // DSN data source name
		//DefaultStringSize: 256, // string 类型字段的默认长度
		//DisableDatetimePrecision: true, // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		//DontSupportRenameIndex: true, // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		//DontSupportRenameColumn: true, // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		//SkipInitializeWithVersion: false, // 根据当前 MySQL 版本自动配置
	}), &gorm.Config{})

	if err != nil {
		fmt.Println(err)
		fmt.Printf("数据库：MySQL启动异常")
		os.Exit(0)
	}
	
	MySqlDB = db

}
