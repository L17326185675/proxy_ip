package main

import (
	"fmt"
	"gitee.com/L17326185675/proxy_ip/service"
	"gitee.com/L17326185675/proxy_ip/tool"
	_var "gitee.com/L17326185675/proxy_ip/var"
	"net/http"
	"time"
)

func main() {
	res, err := http.Get("https://www.baidu.com")
	if err != nil {
		panic(err)
	} else if res.StatusCode != 200 {
		panic("网络不通")
	}

	ser := service.NewService()

	go func() {
		for i := _var.CONFIG.Ipv4Header; i <= 0xFFffFFff; i++ {
			url, err := tool.Long2IPString(uint(i))
			if err != nil {
				return
			}
			for j := 0x0; j <= 0xffff; j++ {
				proxy := fmt.Sprintf("http://%s:%d", url, j)
				ser.Add(proxy)
				for {
					if ser.Addcounts >= _var.CONFIG.ChanelCapacity {
						time.Sleep(time.Second)
					} else {
						break
					}
				}
			}
		}
		fmt.Println("work done")
	}()

	for i := 0; i <= _var.CONFIG.GoroutineNumber; i++ {
		go ser.WorkGetStatusCode()
	}

	ser.Listen()

}
