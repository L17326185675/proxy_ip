module gitee.com/L17326185675/proxy_ip

go 1.16

require (
	github.com/go-resty/resty/v2 v2.7.0
	github.com/kr/pretty v0.2.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.9.0
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/mysql v1.2.0
	gorm.io/gorm v1.22.3
)
