package model

import _var "gitee.com/L17326185675/proxy_ip/var"

type ProxyIP struct {
	Url string

}

func init() {
	_var.MySqlDB.AutoMigrate(&ProxyIP{})
}

func (this *ProxyIP) CreateProxyUrl() error {
	if err := _var.MySqlDB.Create(this).Error; err != nil {
		return err
	}
	return nil
}
