package service

import (
	"fmt"
	"gitee.com/L17326185675/proxy_ip/model"
	_var "gitee.com/L17326185675/proxy_ip/var"
	"github.com/go-resty/resty/v2"
	"log"
	"strings"
)

var Id int64 = 0

type Server struct {
	Addcounts  int
	AddCh      chan string
	addcountCh chan int
}

func NewService() *Server {
	Addcounts := 0
	AddCh := make(chan string, 1000)
	addcountCh := make(chan int)
	return &Server{
		Addcounts,
		AddCh,
		addcountCh,
	}
}

func (s *Server) Add(u string) {
	s.AddCh <- u
	s.addcountCh <- 1
}

func (s *Server) Listen() {
	log.Println("Listening server...")

	for {
		select {
		case <-s.addcountCh:
			s.Addcounts++
		}

	}
}

func (s *Server) WorkGetStatusCode() {
	for {
		proxy := <-s.AddCh//获取IP
		s.Addcounts -= 1 //领取IP数量-1

		if !strings.HasPrefix(proxy, "http://") {
			proxy = fmt.Sprintf("http://%s", proxy)
		}

		client := resty.New()
		client.SetProxy(proxy)
		client.SetHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.17 Safari/537.36")
		resp, _ := client.R().EnableTrace().Get(fmt.Sprintf(_var.CONFIG.PingIp))
		if resp.StatusCode()== 200 {
			fmt.Println(proxy)
			data := &model.ProxyIP{Url: proxy}
			data.CreateProxyUrl()
		}else {
			log.Println("error",proxy)
		}
	}
}
