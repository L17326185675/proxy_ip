package service

import (
	"fmt"
	"gitee.com/L17326185675/proxy_ip/model"
	"gitee.com/L17326185675/proxy_ip/tool"
	"github.com/go-resty/resty/v2"
	"strings"
	"testing"
	"time"
)

func TestName(t *testing.T) {
	ser := NewService()
	for i := 0x0; i <= 0xffffffff; i++ {
		url, err := tool.Long2IPString(uint(i))
		if err != nil {
			return
		}
		for j := 0x0; j <= 0xffff; j++ {
			proxy := fmt.Sprintf("http://%s:%d", url, j)
			go ser.Add(proxy)
			go ser.WorkGetStatusCode()
		}
	}
}
func TestName2(t *testing.T) {
	GetStatusCode2("http://127.0.0.1:9910")
}

func GetStatusCode2(proxy string) {
	if !strings.HasPrefix(proxy, "http://") {
		proxy = fmt.Sprintf("http://%s", proxy)
	}

	client := resty.New()
	client.SetProxy(proxy)
	client.SetHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.17 Safari/537.36")
	resp, _ := client.R().EnableTrace().Get(fmt.Sprintf("https://www.baidu.com"))
	if resp.StatusCode() == 200 {
		fmt.Println(proxy)
		data := &model.ProxyIP{Url: proxy}
		data.CreateProxyUrl()
	}else {
		fmt.Println("error",proxy)
	}
}

func TestLenth(t *testing.T) {
	//var a [10]int
	//a[0] = 1
	//fmt.Println(a)
	//fmt.Println(len(a))
	//fmt.Println(cap(a))
	//fmt.Println("-----")
	//
	//s := a[0:1]
	//s = append(s, 10, 20)
	//fmt.Println(s)
	//fmt.Println(len(s))
	//fmt.Println(cap(s))
	//fmt.Println("------")

	var ch chan string
	ch = make(chan string, 1)
	//ch <- "456152"
	go func() {
		i := 0
		for {
			i++
			ch <- fmt.Sprintf("0---%v",i)
			time.Sleep(10)
		}
	}()
	go func() {
		i := 0
		for {
			i++
			ch <- fmt.Sprintf("1---%v",i)
			time.Sleep(20)
		}

	}()
	go func() {
		i := 0
		for {
			i++
			ch <- fmt.Sprintf("2---%v",i)
			time.Sleep(20)
		}

	}()
	for {
		select {
		case c := <-ch:
			fmt.Println(c)
			fmt.Println("len----",len(ch))
			fmt.Println("cap------",cap(ch))
		}
	}

}

func TestNewService(t *testing.T) {
	i:=0xFF
	fmt.Printf("%T",i)
}
